package inheritance;

public class Parent {
	public void method() {
		System.out.println("This is PARENT method logic.");
	}
	
	public void methodtwo() {
		System.out.println("Method two PARENT method logic");
	}
}
