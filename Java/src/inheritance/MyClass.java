package inheritance;

public class MyClass extends GrandChild {

	public static void main(String[] args) {	
		MyClass mc = new MyClass();
		mc.method();
		mc.methodtwo();
		mc.mymethod();		
		mc.gcmethod();
	}

}