package inheritance;

public class GrandChild extends Child {
	
	public void gcmethod() {
		System.out.println("This is GrandChild");
	}
	
	public static void main (String[] args) {
		GrandChild g = new GrandChild();
		g.method(); //method from Parent
		g.methodtwo(); //method from Parent
		g.mymethod(); //method from Child class
	}

}
