package inheritance;

public class Child extends Parent { //inherited parent class

	public void mymethod() { //its own method
		System.out.println("This is Child Class Method");
	}
	
	public static void main(String[] args) {		
		Child c = new Child(); //object created
		c.method(); //calling parent method
		c.methodtwo();
	}

}
