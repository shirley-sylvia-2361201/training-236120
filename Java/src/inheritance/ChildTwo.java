package inheritance;

public class ChildTwo extends Parent { //Parent class can be used by multiple child class

	public static void main(String[] args) {
		ChildTwo ctwo = new ChildTwo();
		ctwo.method();
		ctwo.methodtwo();

	}

}
