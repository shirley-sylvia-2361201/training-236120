package conditionalStatements;

public class LoopDemo {

	public static void main(String[] args) {
		
		//initialize; give condition; increment or decrement operator
		for(int i=1; i<10;i++) {
			System.out.println(i);
		}
	}

}
