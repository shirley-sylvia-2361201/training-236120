package conditionalStatements;

import java.util.Scanner;

public class ElseIf {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter your age to see if you are eligible for voting:");
		int age = sc.nextInt();
		
		//check eligibility
		if (age>=18 && age<100) {
			System.out.println("\nYou are eligible for voting!");
		}
		else if (age>100) {
			System.out.println("\nYou are not eligible for voting! Too old.");
		}
		else {
			System.out.println("\nYou are not eligible for voting! You are a minor");
		}	

	}

}
