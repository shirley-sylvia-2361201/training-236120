package conditionalStatements;

public class DoWhileLoop {
	
	public static void main(String[] args) {
		
		int a = 1;
		
		//do the block while condition is right
		do {
			System.out.println("Print value a: " + a);
			a++;
		} while(a<=10);
	}

}
