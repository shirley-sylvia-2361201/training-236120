package conditionalStatements;

import java.util.Scanner;

public class GradingElseIf {

	public static void main(String[] args) {
		
		Scanner sc= new Scanner(System.in);
		
		System.out.println("Enter Science Marks: ");
		int science = sc.nextInt();
		System.out.println("Enter Maths Marks: ");
		int math = sc.nextInt();
		System.out.println("Enter Language Marks: ");
		int lang = sc.nextInt();
		
		int total = science+math+lang;
		float avg = total/3;
		
		System.out.println("\nYour average score is: " + avg );
		
		if (avg>=80 && avg<100) {
			System.out.println("Congrats. You secured an 'A'.");
		}
		else if (avg>=60 && avg<80) {
			System.out.println("You can do better next time! You secured a 'B'.");
		}
		else {
			System.out.println("Stop procrastinating!");
		}		

	}

}
