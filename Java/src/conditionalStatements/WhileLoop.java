package conditionalStatements;

public class WhileLoop {

	public static void main(String[] args) {
		
		int a = 1; //initializer, starting point
		
		//check condition first then run code
		//condition, if true then execute the block of code
		while(a<=10) { 
			System.out.println("Print this statement " + a);
			a++;
		}

	}

}