package main;

public class MainMethod { 
	
	void methodone() { 
		System.out.println("This is regular method");
	}
	
	public static void main() { //first method
		System.out.println("This is first method");
	}
	
	public static void main(int a) {
		System.out.println("This is another method");
	}

	public static void main(String[] args) { //execution start from here
		
		//public -- access modifier -- access to all
		//static -- call without any object
		//void -- return type -- it does not return any value
		//main -- name of method
		//String[] -- array of String type arguments
		//args -- just a parameter name, can change	
		
		System.out.println("This is main method");
		
		MainMethod m = new MainMethod();
		m.methodone();	
		
		main();
		main(12);
	}

}
