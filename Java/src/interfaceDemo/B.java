package interfaceDemo;

public class B implements A {

	@Override
	public void methodone() { //method from interface A
		System.out.println("This is interface method.");
	}

	@Override
	public void methodtwo() { //method from interface A
		System.out.println("This is interface method 2");		
	}
	
	void methodthree() { //method from class B
		System.out.println("This is B own method.");
	}
	
	
	public static void main(String[] args) {
		B obj = new B();
		obj.methodone();
		obj.methodtwo();
		obj.methodthree();
	}
}
