package operators;

public class ArithmeticOperator {

	public static void main(String[] args) {
		
		int a=10, b=20;
		
		System.out.println("Addition operator:" + (a+b));
		System.out.println("Subtraction operator:" + (b-a));
		System.out.println("Multiplication operator:" + (a*b));
		System.out.println("Division operator:" + (a/b));
		System.out.println("Modulus operator:" + (a%b));

	}

}
