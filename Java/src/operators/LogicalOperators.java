package operators;

public class LogicalOperators {

	public static void main(String[] args) {
		
		System.out.println((100>50)&&(20>10)); //both value must be true to be true
		System.out.println((10>5||10>12)); //either one need to be true to be true
		System.out.println(10!=2); //two value need to be equal to be true else false
	}

}
