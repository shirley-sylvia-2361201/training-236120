package operators;

public class IncrementDecrement {

	public static void main(String[] args) {
		
		int j = 100;
		System.out.println(j);
		System.out.println(++j);
		System.out.println(--j);
		
		System.out.println("The final value of j is " + j + ".");

	}

}
