package polymorphism;

//creating a parent class
class Vehicle {
	
	//defined method
	void run() {
		System.out.println("Vehicle is running");
	}

}

//creating a child class
class BikeTwo extends Vehicle {
	
	//defining the same method as in the parent class
	void run() {
			System.out.println("Bike is running safely");
	}

	public static void main(String[] args) {
	
		BikeTwo bt = new BikeTwo(); //creating object
		bt.run(); //calling method
			
		Vehicle vc = new Vehicle();
		vc.run();
	
	}

}