package polymorphism;

public class Addition { //method overloading
	
	void sum(int a, int b) { //method created with two parameters
		int c = a + b;
		System.out.println("Total of a an b is: " + c);
	}
	
	//overloading by changing number of arguments
	void sum(int a, int b, int c) { //overloading method 'sum'
		int d = a + b + c;
		System.out.println("The total of a, b and c is: " + d);
	}
	
	//overloading by changing data types
	//can't have method with same number of arguments with same data type
	void sum(double dd, double ff) {
		double rr = dd + ff;
		System.out.println("Total of dd and ff is: " + rr);
	}
	
	public static void main(String[] args) {
		Addition obj = new Addition(); //object created
		obj.sum(55, 66); //two arguments
		obj.sum(20, 30, 40); //three arguments
		obj.sum(50, 50);
	}
}
