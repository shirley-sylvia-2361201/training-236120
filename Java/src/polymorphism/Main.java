package polymorphism;

public class Main {

	public static void main(String[] args) {
		
		Animal a = new Animal();
		a.sound(); //calling parent sound method
		
		Dog d = new Dog();
		d.sound(); //calling child sound methods
		
		Pig p = new Pig();
		p.sound(); 
	}

}
