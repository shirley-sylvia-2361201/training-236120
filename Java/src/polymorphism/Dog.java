package polymorphism;

public class Dog extends Animal { //Dog is child, Animal is parent class
	
	public void sound() {
		System.out.println("I am Dog, this is my own logic.");
	}
	
}
