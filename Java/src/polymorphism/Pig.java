package polymorphism;

public class Pig extends Animal {
	
	public void sound() {
		System.out.println("I am Pig, this is my own logic.");
	}

}
