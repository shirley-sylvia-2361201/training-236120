package staticDemo;

public class StaticMethod {

	void play() { //regular method
		System.out.println("I play Mobile Legend.");
	}
	
	static void sleep() { //static method
		System.out.println("I sleep at 3AM"); //no need to create object to call static method
	}
	
	public static void main(String[] args) {
		
		StaticMethod sm = new StaticMethod();
		
		sleep();
		sm.play();
	}

}
