package staticDemo;

public class StaticBlock {
	
	static {
		
		System.out.println("Checking with main method which one come first");
	
	}

	public static void main(String[] args) {
		
		System.out.println("This is main method");

	}

}
