package abstraction;

public abstract class Animal { //abstract class

	abstract void method(); //abstract method, no method body
	
	void regularMethod() { //regular method
		System.out.println("This is my regular method.");
	}
	
}
