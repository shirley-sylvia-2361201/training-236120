package abstraction;

public class Dog extends Animal {

	@Override
	void method() {
		System.out.println("This is an abstract method, I am providing body.");	
	}
	
}
