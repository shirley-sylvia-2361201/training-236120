package abstraction;

public class Main {

	public static void main(String[] args) {		
		Dog d = new Dog(); //abstract class cannot have object and directly be call
		d.method(); //calling abstract method by using child class object
		d.regularMethod();
	}

}
