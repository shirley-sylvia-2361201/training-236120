package exercise;

import java.util.Scanner;

public class Stat{

	static void Greeting(){
		System.out.println("Hello! How are you?");
	}

	void Calculate(){

		Scanner sc = new Scanner(System.in);
		float arr[]= new float[4];

		System.out.println("Enter four float value: ");
		for(int i=0; i<4; i++){
			arr[i] = sc.nextFloat();
		}

		float sum = arr[0]+arr[1]+arr[2]+arr[3];
		float avg = sum/3;
		
		System.out.println("The average of four value is: " + avg);
	}

	public static void main(String[] args){

		Stat s = new Stat();
		s.Greeting();

		s.Calculate();
		
	}

}