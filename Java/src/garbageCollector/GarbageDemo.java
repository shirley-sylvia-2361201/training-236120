package garbageCollector;

public class GarbageDemo {
	
	//nullifying, reference to another object, anonymous
	
	public void finalize() {
		System.out.println("Unreferenced object garbage collected");
	}

	public static void main(String[] args) {
		
		GarbageDemo gd = new GarbageDemo();
		gd = null; //nullifying		
		GarbageDemo gdtwo = new GarbageDemo();		
		//referencing to another
		gd = gdtwo;
		
		//anonymous object
		new GarbageDemo(); //no name
		System.gc();
						
	}

}
