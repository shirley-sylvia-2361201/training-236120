package collection;

import java.util.LinkedList;
import java.util.Queue;

public class QueueDemo {

	public static void main(String[] args) {
		
		Queue<Integer> numbers = new LinkedList<>();
		
		//offer almost the same as add method
		numbers.offer(101);
		numbers.offer(102);
		numbers.offer(103);
		numbers.offer(104);
		numbers.offer(105);
		System.out.println(numbers);
		
		//retrieve but not replace head of the queue/returns null if queue empty
		int head = numbers.peek();
		System.out.println("\nHead of the queue is: " + head);
		
		//retrieve and removes head of the queue/null if queue empty
		int removednumber = numbers.poll();
		System.out.println("\nRemoved number using poll method is: " + removednumber);
		System.out.println(numbers);
			
	}
}
