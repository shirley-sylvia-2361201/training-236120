package collection;

import java.util.Collections;
import java.util.PriorityQueue;

public class PriorityQueueDemo {

	public static void main(String[] args) {
		
		PriorityQueue<Integer> rollno = new PriorityQueue<>(6, Collections.reverseOrder());//reverse order to print value from high to low
		rollno.offer(205);
		rollno.offer(206);
		rollno.offer(207);
		rollno.offer(208);
		rollno.offer(209);
		rollno.offer(210);
		System.out.println(rollno);
		
		//method remove retrieve and remove head of the queue
		//different from poll in the way it throw exception if the queue is empty
		while(rollno.size()>0) {
			System.out.println(rollno.remove() + " ");
		}
	}

}
