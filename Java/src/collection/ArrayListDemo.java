package collection;

import java.util.ArrayList;

public class ArrayListDemo {

	public static void main(String[] args) {
		
		ArrayList<Object> list = new ArrayList<>();
		
		//adds an elements to a list
		list.add(757);
		list.add(453.22);
		list.add("Ricky");
		list.add(656);
		
		System.out.println(list);
		
		//remove an elements from the list
		//remove one value at a time
		list.remove(3);
		System.out.println(list);

	}

}
