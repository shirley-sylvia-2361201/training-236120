package collection;

import java.util.ArrayList;
import java.util.List;

public class Demo {

	public static void main(String[] args) {
		
		List<String> list = new ArrayList<>();
		
		//add elements
		list.add("New York");
		list.add("Rome");
		list.add("Paris");
		list.add("Copenhagen");
		list.add("Amsterdam");
		list.add("Italy");
		list.add("12345");
		list.add("Rome"); //duplicate value can use array list
		list.add(6, "Stockholm"); //add at index number 6
		list.set(5, "Shanghai"); //replace at index number 5
		System.out.println(list);
		
		//add at particular position/index
		list.add(2, "Canberra");
		System.out.println(list);
		
		//print value add particular position/index
		System.out.println("Capital city of Denmark: " + list.get(3));
		
		//remove value from particular index/position
		list.remove(0);
		System.out.println(list);
		
		//to find size of list
		//assigned variable s or anything
		int s = list.size();
		System.out.println(s);
		
		//to change if the value in list exist or not in the list
		//use boolean, true if exist, false if don't exist
		boolean c = list.contains("Paris");
		System.out.println(c);
		
		List<String> countries = new ArrayList<>();
		countries.add("Malaysia");
		countries.add("Singapore");
		countries.add("South Korea");
		countries.add("Japan");
		System.out.println(countries);
		
		//add countries to list/add list to another list
		countries.addAll(3, list);
		System.out.println("After merging list with countries: " + countries);
	}

}
