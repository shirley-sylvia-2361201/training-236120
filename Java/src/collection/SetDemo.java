package collection;

import java.util.HashSet;
import java.util.Set;

public class SetDemo {
	
	public static void main(String[] args) {
		
		Set<String> sports = new HashSet<>();
		
		sports.add("Tennis");
		sports.add("Badmindtion");
		sports.add("Squash");
		sports.add("Ping Pong");
		System.out.println(sports);
		
		sports.add("Squash");
		System.out.println(sports); //set doesn't allow duplicates
		
		//code to check the size
		//to print out the size assigned it to a variable integer
		int s = sports.size();
		System.out.println(s);
		
		
	}

}
