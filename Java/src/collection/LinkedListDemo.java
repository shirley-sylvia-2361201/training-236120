package collection;

import java.util.LinkedList;

public class LinkedListDemo {

	public static void main(String[] args) {
		
		LinkedList<String> sports = new LinkedList<>();
		
		//add elements
		sports.add("Tennis");
		sports.add("Volley Ball");
		System.out.println(sports);
		
		//add at particular index
		sports.add(0, "Badminton");
		System.out.println(sports);
		
		//add at last index
		sports.addLast("Chess");
		System.out.println(sports);
		
		//retrieve at particular index
		String s = sports.get(1);
		System.out.println(s);
		
		//add at first index
		sports.addFirst("Football");
		System.out.println(sports);

	}

}
