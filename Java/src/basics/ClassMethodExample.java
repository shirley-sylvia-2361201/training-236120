package basics;

public class ClassMethodExample {
	
	//create method
	public void mymethod() {
		System.out.println("This is my first method logic.");
	}
	
	public void mymethod2() {
		System.out.println("This is my second method logic.");
	}


	public static void main(String[] args) {
		
		System.out.println("This is my first Java program.");
		
		//create object
		ClassMethodExample abc = new ClassMethodExample();
		
		abc.mymethod();
		abc.mymethod2();

	}

}
