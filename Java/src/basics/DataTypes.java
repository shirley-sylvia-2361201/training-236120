package basics;

public class DataTypes {

	public static void main(String[] args) {
		
		//primitive data type: char, byte, short, integer, long, float, double, boolean
		int a = 20; 
		byte b = 65;
		short s = 200;
		long l = 46465454;
		float f = 6546.22f; //append f so that the value will be recognized as float otherwise it will be known as double
		double d = 456456566.455d;
		
		System.out.println("The value of a is: " + a);
		System.out.println("The value of b is: " + b);
		System.out.println("The value of s is: " + s);
		System.out.println("The value of l is: " + l);
		System.out.println("The value of f is: " + f);
		System.out.println("The value of d is: " + d);
	}

}
