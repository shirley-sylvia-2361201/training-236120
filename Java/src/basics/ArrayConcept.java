package basics;

public class ArrayConcept {

	public static void main(String[] args) {
		
		//array can store multiple value
		int [] age = {2205, 19, 12, 45, 90, 78, 5, 23, 25};
		System.out.println(age[1]);//print age at index 1
		System.out.println(age.length);//print length of an index
		
		String [] namesofStudents = {"Ricky", "Robert", "Randy"};
		System.out.println(namesofStudents[2]);
		
	}

}
