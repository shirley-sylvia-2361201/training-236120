package thisSuper;

class Animal{
	String animal = "Dog is Parent Class"; //parent class instance variable
}

public class Dog extends Animal {
	
	String animal = "Puppy is Child Class"; //child class instance variable

	void printanimal() {
		String animal = "Retriever"; //local variable
		System.out.println(animal); //this will print local variable
		System.out.println(this.animal); //this will print instance variable
		System.out.println(super.animal); //this will print parent class
	}
	
	public static void main(String[] args) {
		Dog d = new Dog();
		d.printanimal();
		
	}

}
