package thisSuper;

class Parent{
	void play() {
		System.out.println("Parent play mahjong");
	}
}

public class ThisSuperMethod extends Parent{
	
	void play() {
		System.out.println("I play chess");
	}
	
	void all() { //calling current play method and parent play method
		this.play(); //current class play method
		super.play(); //parent class play method
	}
	
	public static void main(String[] args) {
		
		ThisSuperMethod sm = new ThisSuperMethod();
		sm.all();
		
	}
}
