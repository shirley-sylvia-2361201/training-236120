package thisSuper;

class MyParent{
	String name = "This is parent class instance variable";
}

public class MyClass extends MyParent{
	
	String name = "This is an instance variable";
	
	void somemethod() {
		String name = "This is a local variable"; //local variable because inside method
		System.out.println(name);
		System.out.println(this.name);
		System.out.println(super.name);
	}

	public static void main(String[] args) {
		MyClass mc = new MyClass();
		mc.somemethod();
	}
	
}
