package importantFeatures;

public class MethodReferenceTwo {
	
	public static void SomeName() { //Static method
		System.out.println("Hello there.");
	}

	public static void main(String[] args) {
		Thread t = new Thread(MethodReferenceTwo::SomeName);
		t.start(); //Cause thread to begin execution
	}
}
