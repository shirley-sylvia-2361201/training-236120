package importantFeatures;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamDemoTwo {
	
	public static void main(String[] args) {
		//List of names
		List<String> names = Arrays.asList("Ricky","Miley","Lucifer"); //Returns fixed size list
		
		//Convert list to stream by filtering some condition
		List<String> othernames = names.stream().filter(n->!"Ricky".equals(n))
									   .collect(Collectors.toList());
		//Printing the resulted list
		othernames.forEach(System.out::println);
		
		//System.out.println(othernames);
	}

}
