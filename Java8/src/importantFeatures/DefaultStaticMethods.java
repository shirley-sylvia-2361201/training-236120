package importantFeatures;

//Interface contains abstract methods, default method & static methods
interface MyInterface{
	//Abstract method: Method without body (method logic can be write by implementing the class)
	void methodone();
	
	//Static method: Don't need to use object to call it, use class name example: [MyInterface.methodtwo]
	static void methodtwo() {
		System.out.println("This is static method logic.");
	}
	
	//Default method
	default void methodthree() {
		System.out.println("This is default method logic.");
	}
}

//Implementation class because implement interface, the class must implements all methods in interface class
public class DefaultStaticMethods implements MyInterface{

	public static void main(String[] args) {
		DefaultStaticMethods ds = new DefaultStaticMethods();
		ds.methodone(); //Calling abstract method
		MyInterface.methodtwo(); //Calling static method by using interface name
		ds.methodthree(); //Calling default method
	}

	@Override
	public void methodone() {	
		System.out.println("This is overriden abstract method from MyInterface");	
	}
}