package importantFeatures;

interface Cab{
	//Abstract method
	public double method(String start, String destination, int km, double myr);
}

public class LambdaDemoThree {

	public static void main(String[] args) {
		
		Cab cab = (start, destination, km, myr) -> {
			System.out.println("Cab booked from " + start + " to " + destination + "\nTotal Cost:");
			return km*myr;
			};
		
		System.out.print(cab.method("Samarahan", "Kuching.", 20, 1.5));

	}

}
