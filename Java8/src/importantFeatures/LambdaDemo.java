package importantFeatures;

@FunctionalInterface
interface Marathon{
	void methodone(String start, String destination); //Abstract method with some parameters
}

public class LambdaDemo {

	public static void main(String[] args) {
		
		Marathon m = (start, destination) -> System.out.println("I started from " + start + " to " + destination);
		m.methodone("Samarahan", "Kuching.");
	}

}
