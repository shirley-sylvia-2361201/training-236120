package importantFeatures;

/*--Functional Interface: Only one abstract method but can have any number of default and static method.
  --AKA SAM(Single Abstract Method) Interface.
  --Functional interface can be implement by using lambda expression. */

@FunctionalInterface
interface MyFunctionalInterface{
	void methodone();
	
	public static void methodtwo() {
		System.out.println("This is static method.");
	}
}

public class FunctionalInterfaceDemo {
	
	public static void main(String[] args) {
		
		MyFunctionalInterface mf = ()-> 
			System.out.println("This is a demo.");
			mf.methodone();
	
		
	}

}
