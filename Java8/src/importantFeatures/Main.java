package importantFeatures;

import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		List<Person> persons = Arrays.asList(
				new Person("Ricky",25),
				new Person("Luca",30),
				new Person("Santa", 21),
				new Person("Dannielle",23));
		
		//Converting to stream
		Person result = persons.stream().filter(n -> "Luca".equals(n.getName())).findAny().orElse(null);
		System.out.println(result);
	}

}
