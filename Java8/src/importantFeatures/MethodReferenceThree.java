package importantFeatures;

interface MyDisplay{
	public void display(); //Abstract method
}

public class MethodReferenceThree {
	public void greetings() { //Instance method
		System.out.println("Hello there!");
	}
	
	public static void main(String[] args) {
		
		MethodReferenceThree mrt = new MethodReferenceThree(); //Create object
		MyDisplay md = mrt::greetings; //Method reference using object, referring instance method to method abstract (don't use method overriding)
		md.display(); //Calling the functional interface method
	}

}