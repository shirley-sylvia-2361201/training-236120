package importantFeatures;

import java.util.*;

public class ForEachMethod {
	
	public static void main(String[] args) {
		
		List<Integer> n = Arrays.asList(22,545,454,454,555,789,102);
		System.out.println("List of Integers: ");
		n.forEach(System.out::println); //Double quotation, :: is method reference
										//To decrease length of code
										//Don't have to use the method name
	}

}
