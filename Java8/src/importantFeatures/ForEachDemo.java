package importantFeatures;

import java.util.*;

public class ForEachDemo {

	public static void main(String[] args) {	
		 List<String> demo = new ArrayList<>();
		 demo.add("Apple");
		 demo.add("Orange");
		 demo.add("Banana");
		 demo.add("Pineapple");

		 demo.forEach((names)->System.out.println(names));
	}

}
