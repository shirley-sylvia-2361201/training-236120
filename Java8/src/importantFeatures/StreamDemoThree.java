package importantFeatures;

import java.util.Arrays;
import java.util.List;

public class StreamDemoThree {
	
	public static void main(String[] args) {
		
		List<Integer> nums = Arrays.asList(5,6,10,13,3,2);
		
		//Can be written in single line also
		nums.stream()
			.sorted() //Sort the integer value
			.forEach(n->System.out.print(" " + n)); //Print each element in the array
		
		System.out.println("\n================");		
		nums.stream()
			.filter(n->n%2==1)
			.forEach(n->System.out.print(" " + n));
		
		System.out.println("\n================");		
		nums.stream()
			.filter(n->n%2==1)
			.sorted()
			.forEach(n->System.out.print(" " + n));
		
		System.out.println("\n================");		
		nums.stream()
			.map(n->n*2)
			.sorted()
			.forEach(n->System.out.print(" " + n));
		
	}

}
