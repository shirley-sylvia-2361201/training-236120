package importantFeatures;

@FunctionalInterface
interface Cat{
	public void methodsound();
}

public class Lambda{ //Do not need to use implement also okay if use lambda expression

	public static void main(String[] args) {
		Cat c = ()-> System.out.println("Meow meow");
		c.methodsound();
	}

}