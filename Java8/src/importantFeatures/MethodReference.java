package importantFeatures;

interface A{
	void getName(String name); //abstract method
}

public class MethodReference {

	public static void main(String[] args) {	
		//Lambda expression
		A ref = (String n) -> System.out.println(n);
		ref.getName("Shirley");
		
		//Method reference

	}

}
