package importantFeatures;

//Functional Interface
@FunctionalInterface
interface Calculation {
	//Abstract method
	public int multiply (int a, int b);
	
	//Static method
	public static int addition (int c, int d) {
		int add = c + d;
		return add;
	}
	
	//Default method
	public default void division () {
		int z = 12;
		int	x = 6;
		int div = z/x;
		System.out.println("Division value: " + div);
	}
}

class LambdaExercise {
	
   public static void main(String args[])  {
	   
	   //Lambda expression to implement functional interface
	   Calculation c = (a, b)-> a * b; //a*b Logic for multiplication abstract method
	   int mul = c.multiply(10, 100);
	   System.out.println("Multiplication value: " + mul);
	   
	   
	   int add = Calculation.addition(100, 200); //To call static method use class name.method
	   System.out.println("Addition value: " + add);
	   
	   c.division();
	   
	   
   }
}
