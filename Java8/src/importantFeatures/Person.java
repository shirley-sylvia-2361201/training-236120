package importantFeatures;

//Object class
public class Person {
	
	private String name;
	private int age;
	
	//Constructor using fields
	public Person(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	//To access private variable by other variable use getter and setter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + "]";
	}

}
