package importantFeatures;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class StreamDemo {
	
	public static void main(String[] args) {
		
		List<String> names = new ArrayList <>();
		names.add("Anne");
		names.add("Karen");
		names.add("Cat");
		names.add("Olive");
		/*
		int count = 0;
		for(String str:names) {
			if(str.length()<5)
				count++;
		}
		
		System.out.println(count + " names with less than four characters"); */
		
		//Streams & Lambda
		long count = names.stream().filter(str->str.length()<5).count();
		System.out.println(count + " names with less than five characters.");
		
		//Stream using stream.of
		Stream<String> namestwo = Stream.of("Ricky","Lucky");
		namestwo.forEach(System.out::println);
		
	}

}
